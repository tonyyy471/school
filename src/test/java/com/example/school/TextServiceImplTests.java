package com.example.school;

import com.example.school.exceptions.NotAcceptableException;
import com.example.school.exceptions.NotExistingException;
import com.example.school.models.Text;
import com.example.school.models.dtos.TextDTO;
import com.example.school.repositories.TextRepositoryImpl;
import com.example.school.services.TextServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;

@RunWith(MockitoJUnitRunner.class)
public class TextServiceImplTests {
    @Mock
    TextRepositoryImpl textRepository;
    @InjectMocks
    TextServiceImpl textService;

    @Test
    public void create_Should_Call_RepositorySave() throws SQLException {
        Text mockText = new Text("Random content");

        textService.create(mockText);

        Mockito.verify(textRepository, Mockito.times(1)).save(mockText);
    }

    @Test(expected = NotAcceptableException.class)
    public void create_Should_ThrowException_When_ContentIsNullEmptyOrBlank() throws SQLException {
        Text mockText = new Text("");

        textService.create(mockText);

        Mockito.verify(textRepository, Mockito.never()).save(mockText);
    }

    @Test
    public void edit_Should_Call_RepositoryUpdate() throws SQLException {
        Text mockText = new Text("Random content");
        Mockito.when(textRepository.getById(1)).thenReturn(new TextDTO(1, "Random content"));

        textService.edit(1, mockText);

        Mockito.verify(textRepository, Mockito.times(1)).update(1, mockText);
    }

    @Test(expected = NotAcceptableException.class)
    public void edit_Should_ThrowException_When_ContentIsNullEmptyOrBlank() throws SQLException {
        Text mockText = new Text("");

        textService.edit(1, mockText);

        Mockito.verify(textRepository, Mockito.never()).update(1, mockText);
    }

    @Test(expected = NotExistingException.class)
    public void edit_Should_ThrowException_When_TextDoesntExist() throws SQLException {
        Text mockText = new Text("Random content");
        Mockito.when(textRepository.getById(1)).thenReturn(null);

        textService.edit(1, mockText);

        Mockito.verify(textRepository, Mockito.never()).update(1, mockText);
    }

    @Test
    public void getById_Should_ReturnText() throws SQLException {
        TextDTO mockText = new TextDTO(1, "Random content");
        Mockito.when(textRepository.getById(1)).thenReturn(mockText);

        TextDTO textDTO = textRepository.getById(1);

        Assert.assertEquals(textDTO, mockText);
    }

    @Test(expected = NotExistingException.class)
    public void getById_Should_ThrowException_When_TextDoesntExist() throws SQLException {
        TextDTO mockText = null;
        Mockito.when(textRepository.getById(1)).thenReturn(mockText);

        TextDTO textDTO = textService.getById(1);

        Assert.assertEquals(textDTO, mockText);
    }

    @Test
    public void delete_Should_Call_RepositoryDelete() throws SQLException {
        Mockito.when(textRepository.getById(1)).thenReturn(new TextDTO(1, "Random content"));

        textService.delete(1);

        Mockito.verify(textRepository, Mockito.times(1)).delete(1);
    }

    @Test(expected = NotExistingException.class)
    public void delete_Should_ThrowException_When_Text_DoesntExist() throws SQLException {
        Mockito.when(textRepository.getById(1)).thenReturn(null);

        textService.delete(1);

        Mockito.verify(textRepository, Mockito.never()).delete(1);
    }
}
