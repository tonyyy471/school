package com.example.school;

import com.example.school.exceptions.NoContentException;
import com.example.school.exceptions.NotAcceptableException;
import com.example.school.exceptions.NotExistingException;
import com.example.school.models.Category;
import com.example.school.models.dtos.CategoryDTO;
import com.example.school.repositories.CategoryRepositoryImpl;
import com.example.school.services.CategoryServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceImplTests {
    @Mock
    CategoryRepositoryImpl categoryRepository;
    @InjectMocks
    CategoryServiceImpl categoryService;

    @Test
    public void getAll_Should_ReturnAllCategories() throws SQLException {
        List<CategoryDTO> mockCategories = Arrays.asList(new CategoryDTO(1, "News"), new CategoryDTO(2, "Новини"),
                new CategoryDTO(3, "Random category"));
        Mockito.when(categoryRepository.getAll()).thenReturn(mockCategories);

        List<CategoryDTO> categories = categoryService.getAll();

        Assert.assertEquals(categories, mockCategories);
    }

    @Test(expected = NoContentException.class)
    public void getAll_Should_ThrowException_When_CategoriesDontExist() throws SQLException {
        List<CategoryDTO> mockCategories = new ArrayList<>();
        Mockito.when(categoryRepository.getAll()).thenReturn(mockCategories);

        List<CategoryDTO> categories = categoryService.getAll();

        Assert.assertEquals(categories, mockCategories);
    }

    @Test
    public void create_Should_Call_RepositorySave() throws SQLException {
        Category mockCategory = new Category("Random category");
        Mockito.when(categoryRepository.getByName(mockCategory)).thenReturn(null);

        categoryService.create(mockCategory);

        Mockito.verify(categoryRepository, Mockito.times(1)).save(mockCategory);
    }

    @Test(expected = NotAcceptableException.class)
    public void create_Should_ThrowException_When_NameIsNullEmptyOrBlank() throws SQLException {
        Category mockCategory = new Category(" ");

        categoryService.create(mockCategory);

        Mockito.verify(categoryRepository, Mockito.never()).save(mockCategory);
    }

    @Test(expected = NotAcceptableException.class)
    public void create_Should_ThrowException_When_CategoryExists() throws SQLException {
        Category mockCategory = new Category("Random category");
        Mockito.when(categoryRepository.getByName(mockCategory)).thenReturn(new CategoryDTO(1, mockCategory.getName()));

        categoryService.create(mockCategory);

        Mockito.verify(categoryRepository, Mockito.never()).save(mockCategory);
    }

    @Test
    public void edit_Should_Call_RepositoryUpdate() throws SQLException {
        Category mockCategory = new Category("Random category");
        Mockito.when(categoryRepository.getById(1)).thenReturn(new CategoryDTO(1, "Random name"));

        categoryService.edit(1, mockCategory);

        Mockito.verify(categoryRepository, Mockito.times(1)).update(1, mockCategory);
    }

    @Test(expected = NotAcceptableException.class)
    public void edit_Should_ThrowException_When_NameIsNullEmptyOrBlank() throws SQLException {
        Category mockCategory = new Category(null);

        categoryService.edit(1, mockCategory);

        Mockito.verify(categoryRepository, Mockito.never()).update(1, mockCategory);
    }

    @Test(expected = NotExistingException.class)
    public void edit_Should_ThrowException_When_CategoryDoesntExist() throws SQLException {
        Category mockCategory = new Category("Random category");
        Mockito.when(categoryRepository.getById(1)).thenReturn(null);

        categoryService.edit(1, mockCategory);

        Mockito.verify(categoryRepository, Mockito.never()).update(1, mockCategory);
    }

    @Test
    public void delete_Should_Call_RepositoryDelete() throws SQLException {
        Mockito.when(categoryRepository.getById(1)).thenReturn(new CategoryDTO(1, "Random name"));

        categoryService.delete(1);

        Mockito.verify(categoryRepository, Mockito.times(1)).delete(1);
    }

    @Test(expected = NotExistingException.class)
    public void delete_Should_ThrowException_When_CategoryDoesntExist() throws SQLException {
        Mockito.when(categoryRepository.getById(1)).thenReturn(null);

        categoryService.delete(1);

        Mockito.verify(categoryRepository, Mockito.never()).delete(1);
    }
}
