package com.example.school;

import com.example.school.exceptions.NoContentException;
import com.example.school.exceptions.NotAcceptableException;
import com.example.school.exceptions.NotExistingException;
import com.example.school.models.Article;
import com.example.school.models.dtos.ArticleDTO;
import com.example.school.models.dtos.CategoryDTO;
import com.example.school.repositories.ArticleRepositoryImpl;
import com.example.school.repositories.CategoryRepositoryImpl;
import com.example.school.repositories.TextRepositoryImpl;
import com.example.school.services.ArticleServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ArticleServiceImplTests {
    @Mock
    ArticleRepositoryImpl articleRepository;
    @Mock
    CategoryRepositoryImpl categoryRepository;
    @Mock
    TextRepositoryImpl textRepository;
    @InjectMocks
    ArticleServiceImpl articleService;

    @Test
    public void create_Should_Call_RepositorySave() throws SQLException {
        Article article = new Article("Random title", 1, null);
        Mockito.when(categoryRepository.getById(1)).thenReturn(new CategoryDTO(1, "Random category"));

        articleService.create(article);

        Mockito.verify(articleRepository, Mockito.times(1)).save(article);
    }

    @Test(expected = NotAcceptableException.class)
    public void create_Should_ThrowException_When_ArticleTitleIsNullEmptyOrBlank() throws SQLException {
        Article article = new Article("", 1, null);

        articleService.create(article);

        Mockito.verify(articleRepository, Mockito.never()).save(article);
    }

    @Test(expected = NotAcceptableException.class)
    public void create_Should_ThrowException_When_CategoryIdIsNotProvided() throws SQLException {
        Article article = new Article("Random title", null, null);

        articleService.create(article);

        Mockito.verify(articleRepository, Mockito.never()).save(article);
    }

    @Test(expected = NotExistingException.class)
    public void create_Should_ThrowException_When_CategoryDoesntExist() throws SQLException {
        Article article = new Article("Random title", 1, null);
        Mockito.when(categoryRepository.getById(1)).thenReturn(null);

        articleService.create(article);

        Mockito.verify(articleRepository, Mockito.never()).save(article);
    }

    @Test(expected = NotExistingException.class)
    public void create_Should_ThrowException_When_TextIdIsProvided_And_TextDoesntExist() throws SQLException {
        Article article = new Article("Random title", 1, 1);
        Mockito.when(categoryRepository.getById(1)).thenReturn(new CategoryDTO(1, "Random category"));
        Mockito.when(textRepository.getById(1)).thenReturn(null);

        articleService.create(article);

        Mockito.verify(articleRepository, Mockito.never()).save(article);
    }

    @Test
    public void edit_Should_Call_RepositoryUpdate() throws SQLException {
        Article article = new Article(null, null, null);
        Mockito.when(articleRepository.getById(1)).thenReturn(new ArticleDTO(1, null, null, null, null));

        articleService.edit(1, article);

        Mockito.verify(articleRepository, Mockito.times(1)).update(1, article);
    }

    @Test(expected = NotExistingException.class)
    public void edit_Should_ThrowException_When_ArticleDoesntExist() throws SQLException {
        Article article = new Article("Random title", null, null);
        Mockito.when(articleRepository.getById(1)).thenReturn(null);

        articleService.edit(1, article);

        Mockito.verify(articleRepository, Mockito.never()).update(1, article);
    }

    @Test(expected = NotAcceptableException.class)
    public void edit_Should_ThrowException_When_ArticleTitleIsProvided_And_ArticleTitleIsEmptyOrBlank() throws SQLException {
        Article article = new Article(" ", null, null);
        Mockito.when(articleRepository.getById(1)).thenReturn(new ArticleDTO(1, null, null, null, null));

        articleService.edit(1, article);

        Mockito.verify(articleRepository, Mockito.never()).update(1, article);
    }

    @Test(expected = NotExistingException.class)
    public void edit_Should_ThrowException_When_CategoryIdIsProvided_And_CategoryDoesntExist() throws SQLException {
        Article article = new Article("Random title", 1, null);
        Mockito.when(articleRepository.getById(1)).thenReturn(new ArticleDTO(1, null, null, null, null));
        Mockito.when(categoryRepository.getById(1)).thenReturn(null);

        articleService.edit(1, article);

        Mockito.verify(articleRepository, Mockito.never()).update(1, article);
    }

    @Test(expected = NotExistingException.class)
    public void edit_Should_ThrowException_When_TextIdIsProvided_And_TextDoesntExist() throws SQLException {
        Article article = new Article("Random title", 1, 1);
        Mockito.when(articleRepository.getById(1)).thenReturn(new ArticleDTO(1, null, null, null, null));
        Mockito.when(categoryRepository.getById(1)).thenReturn(new CategoryDTO(1, "Random category"));
        Mockito.when(textRepository.getById(1)).thenReturn(null);

        articleService.edit(1, article);

        Mockito.verify(articleRepository, Mockito.never()).update(1, article);
    }

    @Test
    public void delete_Should_Call_RepositoryDelete() throws SQLException {
        Mockito.when(articleRepository.getById(1)).thenReturn(new ArticleDTO(1, null, null, null, null));

        articleService.delete(1);

        Mockito.verify(articleRepository, Mockito.times(1)).delete(1);
    }

    @Test(expected = NotExistingException.class)
    public void delete_Should_ThrowException_When_ArticleDoesntExist() throws SQLException {
        Mockito.when(articleRepository.getById(1)).thenReturn(null);

        articleService.delete(1);

        Mockito.verify(articleRepository, Mockito.never()).delete(1);
    }

    @Test
    public void getLastFive_Should_ReturnLastFIveArticles() throws SQLException {
        List<ArticleDTO> mockArticles = Arrays.asList(new ArticleDTO(1, null, null, null, null),
                new ArticleDTO(2, null, null, null, null), new ArticleDTO(3, null, null, null, null),
                new ArticleDTO(4, null, null, null, null), new ArticleDTO(5, null, null, null, null));
        Mockito.when(articleRepository.getLastFive()).thenReturn(mockArticles);

        List<ArticleDTO> articleDTOS = articleService.getLastFive();

        Assert.assertEquals(articleDTOS, mockArticles);
    }

    @Test(expected = NoContentException.class)
    public void getLastFive_Should_ThrowException_When_ArticlesDontExist() throws SQLException {
        List<ArticleDTO> mockArticles = new ArrayList<>();
        Mockito.when(articleRepository.getLastFive()).thenReturn(mockArticles);

        List<ArticleDTO> articleDTOS = articleService.getLastFive();

        Assert.assertEquals(articleDTOS, mockArticles);
    }

    @Test
    public void getForCategory_Should_ReturnArticlesForCategory() throws SQLException {
        List<ArticleDTO> mockArticles = Arrays.asList(new ArticleDTO(1, null, null, 1, null),
                new ArticleDTO(2, null, null, 1, null));
        Mockito.when(categoryRepository.getById(1)).thenReturn(new CategoryDTO(1, "Random category"));
        Mockito.when(articleRepository.getForCategory(1)).thenReturn(mockArticles);

        List<ArticleDTO> articleDTOS = articleService.getForCategory(1);

        Assert.assertEquals(articleDTOS, mockArticles);
    }

    @Test(expected = NotExistingException.class)
    public void getForCategory_Should_ThrowException_When_CategoryDoesntExist() throws SQLException {
        List<ArticleDTO> mockArticles = Arrays.asList(new ArticleDTO(1, null, null, 1, null),
                new ArticleDTO(2, null, null, 1, null));
        Mockito.when(categoryRepository.getById(1)).thenReturn(null);

        List<ArticleDTO> articleDTOS = articleService.getForCategory(1);

        Assert.assertEquals(articleDTOS, mockArticles);
    }

    @Test(expected = NoContentException.class)
    public void getForCategory_Should_ThrowException_When_ArticlesDontExist() throws SQLException {
        List<ArticleDTO> mockArticles = new ArrayList<>();
        Mockito.when(categoryRepository.getById(1)).thenReturn(new CategoryDTO(1, "Random category"));
        Mockito.when(articleRepository.getForCategory(1)).thenReturn(mockArticles);

        List<ArticleDTO> articleDTOS = articleService.getForCategory(1);

        Assert.assertEquals(articleDTOS, mockArticles);
    }

    @Test
    public void getForMonth_Should_ReturnArticlesForMonth() throws SQLException, ParseException {
        List<ArticleDTO> mockArticles = Arrays.asList(new ArticleDTO(1, null, null, 1, null),
                new ArticleDTO(2, null, null, 1, null));
        Mockito.when(articleRepository.getForMonth(2018, 8)).thenReturn(mockArticles);

        List<ArticleDTO> articleDTOS = articleService.getForMonth(2018, 8);

        Assert.assertEquals(articleDTOS, mockArticles);
    }

    @Test(expected = NotAcceptableException.class)
    public void getForMonth_Should_ThrowException_When_MonthIsInvalid() throws SQLException, ParseException {
        List<ArticleDTO> mockArticles = Arrays.asList(new ArticleDTO(1, null, null, 1, null),
                new ArticleDTO(2, null, null, 1, null));

        List<ArticleDTO> articleDTOS = articleService.getForMonth(2018, 17);

        Assert.assertEquals(articleDTOS, mockArticles);
    }

    @Test(expected = NoContentException.class)
    public void getForMonth_Should_ThrowException_When_ArticlesDontExist() throws SQLException, ParseException {
        List<ArticleDTO> mockArticles = new ArrayList<>();
        Mockito.when(articleRepository.getForMonth(2018, 8)).thenReturn(mockArticles);

        List<ArticleDTO> articleDTOS = articleService.getForMonth(2018, 8);

        Assert.assertEquals(articleDTOS, mockArticles);
    }
}
