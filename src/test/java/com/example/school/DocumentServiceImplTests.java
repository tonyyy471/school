package com.example.school;

import com.example.school.exceptions.NoContentException;
import com.example.school.exceptions.NotAcceptableException;
import com.example.school.exceptions.NotExistingException;
import com.example.school.models.Document;
import com.example.school.models.dtos.ArticleDTO;
import com.example.school.models.dtos.DocumentDTO;
import com.example.school.models.dtos.TextDTO;
import com.example.school.repositories.ArticleRepositoryImpl;
import com.example.school.repositories.DocumentRepositoryImpl;
import com.example.school.repositories.TextRepositoryImpl;
import com.example.school.services.DocumentServiceImpl;
import com.example.school.services.FileServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class DocumentServiceImplTests {
    @Mock
    DocumentRepositoryImpl documentRepository;
    @Mock
    FileServiceImpl fileService;
    @Mock
    TextRepositoryImpl textRepository;
    @Mock
    ArticleRepositoryImpl articleRepository;
    @InjectMocks
    DocumentServiceImpl documentService;

    @Test
    public void create_Should_Call_RepositorySave() throws SQLException {
        Document document = new Document("Random title", "Random fileName", null, 1);
        Mockito.when(fileService.getFileBytesForName("Random fileName")).thenReturn(new byte[1]);
        Mockito.when(articleRepository.getById(1)).thenReturn(new ArticleDTO(1, null, null, null, null));

        documentService.create(document);

        Mockito.verify(documentRepository, Mockito.times(1)).save(document);
    }

    @Test(expected = NotAcceptableException.class)
    public void create_Should_ThrowException_When_TitleIsNullEmptyOrBlank() throws SQLException {
        Document document = new Document("", "Random fileName", null, 1);

        documentService.create(document);

        Mockito.verify(documentRepository, Mockito.never()).save(document);
    }

    @Test(expected = NotAcceptableException.class)
    public void create_Should_ThrowException_When_FileNameIsNotProvided() throws SQLException {
        Document document = new Document("Random title", null, null, 1);

        documentService.create(document);

        Mockito.verify(documentRepository, Mockito.never()).save(document);
    }

    @Test(expected = NotExistingException.class)
    public void create_Should_ThrowException_When_FileDoesntExist() throws SQLException {
        Document document = new Document("Random title", "Random fileName", null, 1);
        Mockito.when(fileService.getFileBytesForName("Random fileName")).thenReturn(null);

        documentService.create(document);

        Mockito.verify(documentRepository, Mockito.never()).save(document);
    }

    @Test(expected = NotExistingException.class)
    public void create_Should_ThrowException_When_TextIdIsNotNull_And_TextDoesntExist() throws SQLException {
        Document document = new Document("Random title", "Random fileName", 1, 1);
        Mockito.when(fileService.getFileBytesForName("Random fileName")).thenReturn(new byte[1]);
        Mockito.when(textRepository.getById(1)).thenReturn(null);

        documentService.create(document);

        Mockito.verify(documentRepository, Mockito.never()).save(document);
    }

    @Test(expected = NotAcceptableException.class)
    public void create_Should_ThrowException_When_ArticleIdIsNotProvided() throws SQLException {
        Document document = new Document("Random title", "Random fileName", 1, null);
        Mockito.when(fileService.getFileBytesForName("Random fileName")).thenReturn(new byte[1]);
        Mockito.when(textRepository.getById(1)).thenReturn(new TextDTO(1, "Random content"));

        documentService.create(document);

        Mockito.verify(documentRepository, Mockito.never()).save(document);
    }

    @Test(expected = NotExistingException.class)
    public void create_Should_ThrowException_When_ArticleDoesntExist() throws SQLException {
        Document document = new Document("Random title", "Random fileName", 1, 1);
        Mockito.when(fileService.getFileBytesForName("Random fileName")).thenReturn(new byte[1]);
        Mockito.when(textRepository.getById(1)).thenReturn(new TextDTO(1, "Random content"));
        Mockito.when(articleRepository.getById(1)).thenReturn(null);

        documentService.create(document);

        Mockito.verify(documentRepository, Mockito.never()).save(document);
    }

    @Test
    public void edit_Should_Call_RepositoryUpdate() throws SQLException {
        Document document = new Document(null, null, null, null);
        Mockito.when(documentRepository.getById(1))
                .thenReturn(new DocumentDTO(1, "Random title", "Random fileName", new byte[1], 1, 1));

        documentService.edit(1, document);

        Mockito.verify(documentRepository, Mockito.times(1)).update(1, document);
    }

    @Test(expected = NotExistingException.class)
    public void edit_Should_ThrowException_When_DocumentDoesntExist() throws SQLException {
        Document document = new Document("Random title", "Random fileName", 1, 1);
        Mockito.when(documentRepository.getById(1)).thenReturn(null);

        documentService.edit(1, document);

        Mockito.verify(documentRepository, Mockito.never()).update(1, document);
    }

    @Test(expected = NotAcceptableException.class)
    public void edit_Should_ThrowException_When_TitleIsNotNull_And_TitleIsEmptyOrBlank() throws SQLException {
        Document document = new Document("", null, null, null);
        Mockito.when(documentRepository.getById(1))
                .thenReturn(new DocumentDTO(1, "Random title", "Random fileName", new byte[1], 1, 1));

        documentService.edit(1, document);

        Mockito.verify(documentRepository, Mockito.never()).update(1, document);
    }

    @Test(expected = NotExistingException.class)
    public void edit_Should_ThrowException_When_FileNameIsNotNull_And_FileDoesntExist() throws SQLException {
        Document document = new Document(null, "Random filename", null, null);
        Mockito.when(documentRepository.getById(1))
                .thenReturn(new DocumentDTO(1, "Random title", "Random fileName", new byte[1], 1, 1));
        Mockito.when(fileService.getFileBytesForName("Random filename")).thenReturn(null);

        documentService.edit(1, document);

        Mockito.verify(documentRepository, Mockito.never()).update(1, document);
    }

    @Test(expected = NotExistingException.class)
    public void edit_Should_ThrowException_When_TextIdIsNotNull_And_TextDoesntExist() throws SQLException {
        Document document = new Document(null, null, 1, null);
        Mockito.when(documentRepository.getById(1))
                .thenReturn(new DocumentDTO(1, "Random title", "Random fileName", new byte[1], 1, 1));
        Mockito.when(textRepository.getById(1)).thenReturn(null);

        documentService.edit(1, document);

        Mockito.verify(documentRepository, Mockito.never()).update(1, document);
    }

    @Test(expected = NotExistingException.class)
    public void edit_Should_ThrowException_When_ArticleIdIsNotNull_And_ArticleDoesntExist() throws SQLException {
        Document document = new Document(null, null, null, 1);
        Mockito.when(documentRepository.getById(1))
                .thenReturn(new DocumentDTO(1, "Random title", "Random fileName", new byte[1], 1, 1));
        Mockito.when(articleRepository.getById(1)).thenReturn(null);

        documentService.edit(1, document);

        Mockito.verify(documentRepository, Mockito.never()).update(1, document);
    }

    @Test
    public void delete_Should_Call_RepositoryDelete() throws SQLException {
        Mockito.when(documentRepository.getById(1))
                .thenReturn(new DocumentDTO(1, "Random title", "Random fileName", new byte[1], 1, 1));

        documentService.delete(1);

        Mockito.verify(documentRepository, Mockito.times(1)).delete(1);
    }

    @Test(expected = NotExistingException.class)
    public void delete_Should_Throw_Exception_When_DocumentDoesntExist() throws SQLException {
        Mockito.when(documentRepository.getById(1)).thenReturn(null);

        documentService.delete(1);

        Mockito.verify(documentRepository, Mockito.never()).delete(1);
    }

    @Test
    public void getForArticle_Should_ReturnDocumentsForArticle() throws SQLException {
        List<DocumentDTO> mockDocuments = Arrays.asList(new DocumentDTO(1, null, null, null, null, null),
                new DocumentDTO(2, null, null, null, null, null), new DocumentDTO(3, null, null, null, null, null));
        Mockito.when(documentRepository.getForArticle(1)).thenReturn(mockDocuments);
        Mockito.when(articleRepository.getById(1)).thenReturn(new ArticleDTO(1, null, null, null, null));

        List<DocumentDTO> documentDTOS = documentService.getForArticle(1);

        Assert.assertEquals(mockDocuments, documentDTOS);
    }

    @Test(expected = NotExistingException.class)
    public void getForArticle_Should_Throw_Exception_When_ArticleDoesntExist() throws SQLException {
        List<DocumentDTO> mockDocuments = Arrays.asList(new DocumentDTO(1, null, null, null, null, null),
                new DocumentDTO(2, null, null, null, null, null), new DocumentDTO(3, null, null, null, null, null));
        Mockito.when(documentRepository.getForArticle(1)).thenReturn(mockDocuments);
        Mockito.when(articleRepository.getById(1)).thenReturn(null);

        List<DocumentDTO> documentDTOS = documentService.getForArticle(1);

        Assert.assertEquals(mockDocuments, documentDTOS);
    }

    @Test(expected = NoContentException.class)
    public void getForArticle_Should_Throw_Exception_When_DocumentsDontExist() throws SQLException {
        List<DocumentDTO> mockDocuments = new ArrayList<>();
        Mockito.when(documentRepository.getForArticle(1)).thenReturn(mockDocuments);
        Mockito.when(articleRepository.getById(1)).thenReturn(new ArticleDTO(1, null, null, null, null));

        List<DocumentDTO> documentDTOS = documentService.getForArticle(1);

        Assert.assertEquals(mockDocuments, documentDTOS);
    }
}
