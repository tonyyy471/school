package com.example.school.services;

import com.example.school.models.Image;
import com.example.school.models.dtos.ImageDTO;
import com.example.school.repositories.ArticleRepository;
import com.example.school.repositories.ImageRepository;
import com.example.school.repositories.TextRepository;
import com.example.school.services.utils.Verifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {
    private final ImageRepository imageRepository;
    private final FileService fileService;
    private final TextRepository textRepository;
    private final ArticleRepository articleRepository;

    @Autowired
    public ImageServiceImpl(ImageRepository imageRepository, FileService fileService, TextRepository textRepository, ArticleRepository articleRepository) {
        this.imageRepository = imageRepository;
        this.fileService = fileService;
        this.textRepository = textRepository;
        this.articleRepository = articleRepository;
    }

    @Override
    public void create(Image image) throws SQLException {
        if (hasTitle(image))
            Verifier.getInstance().verifyIsNotNullEmptyOrBlank(image.getTitle(), "Image title");

        Verifier.getInstance().verifyIsProvided(image.getFileName(), "File name");
        Verifier.getInstance().verifyExists(fileService.getFileBytesForName(image.getFileName()), "File with name " + image.getFileName());

        if (hasText(image))
            Verifier.getInstance().verifyExists(textRepository.getById(image.getTextId()), "Text with id " + image.getTextId());

        Verifier.getInstance().verifyIsProvided(image.getArticleId(), "Article id");
        Verifier.getInstance().verifyExists(articleRepository.getById(image.getArticleId()), "Article with id " + image.getArticleId());

        imageRepository.save(image);
    }

    @Override
    public void edit(Integer id, Image image) throws SQLException {
        Verifier.getInstance().verifyExists(imageRepository.getById(id), "Image");

        if (hasTitle(image))
            Verifier.getInstance().verifyIsNotNullEmptyOrBlank(image.getTitle(), "Image title");
        if (hasFile(image))
            Verifier.getInstance().verifyExists(fileService.getFileBytesForName(image.getFileName()), "File with name " + image.getFileName());
        if (hasText(image))
            Verifier.getInstance().verifyExists(textRepository.getById(image.getTextId()), "Text with id " + image.getTextId());
        if (hasArticle(image))
            Verifier.getInstance().verifyExists(articleRepository.getById(image.getArticleId()), "Article with id " + image.getArticleId());

        imageRepository.update(id, image);
    }

    @Override
    public void delete(Integer id) throws SQLException {
        Verifier.getInstance().verifyExists(imageRepository.getById(id), "Image");

        imageRepository.delete(id);
    }

    @Override
    public List<ImageDTO> getForArticle(Integer id) throws SQLException {
        Verifier.getInstance().verifyExists(articleRepository.getById(id), "Article with id " + id);

        List<ImageDTO> imageDTOS = imageRepository.getForArticle(id);
        Verifier.getInstance().verifySystemHasAny(imageDTOS, "images for article with id " + id);

        imageDTOS.forEach(imageDTO -> imageDTO.setFileBytes(fileService.getFileBytesForName(imageDTO.getFileName())));

        return imageDTOS;
    }

    private boolean hasTitle(Image image) {
        return image.getTitle() != null;
    }

    private boolean hasFile(Image image) {
        return image.getFileName() != null;
    }

    private boolean hasText(Image image) {
        return image.getTextId() != null;
    }

    private boolean hasArticle(Image image) {
        return image.getArticleId() != null;
    }
}
