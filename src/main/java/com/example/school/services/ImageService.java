package com.example.school.services;

import com.example.school.models.Image;
import com.example.school.models.dtos.ImageDTO;

import java.sql.SQLException;
import java.util.List;

public interface ImageService {
    void create(Image image) throws SQLException;

    void edit(Integer id, Image image) throws SQLException;

    void delete(Integer id) throws SQLException;

    List<ImageDTO> getForArticle(Integer id) throws SQLException ;
}
