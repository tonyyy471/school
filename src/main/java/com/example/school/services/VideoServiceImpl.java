package com.example.school.services;

import com.example.school.models.Video;
import com.example.school.models.dtos.VideoDTO;
import com.example.school.repositories.ArticleRepository;
import com.example.school.repositories.TextRepository;
import com.example.school.repositories.VideoRepository;
import com.example.school.services.utils.Verifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {
    private final VideoRepository videoRepository;
    private final TextRepository textRepository;
    private final ArticleRepository articleRepository;

    @Autowired
    public VideoServiceImpl(VideoRepository videoRepository, TextRepository textRepository, ArticleRepository articleRepository) {
        this.videoRepository = videoRepository;
        this.textRepository = textRepository;
        this.articleRepository = articleRepository;
    }

    @Override
    public void create(Video video) throws SQLException {
        if (hasTitle(video))
            Verifier.getInstance().verifyIsNotNullEmptyOrBlank(video.getTitle(), "Video title");

        Verifier.getInstance().verifyIsNotNullEmptyOrBlank(video.getUrl(), "Video url");

        if (hasText(video))
            Verifier.getInstance().verifyExists(textRepository.getById(video.getTextId()), "Text with id " + video.getTextId());

        Verifier.getInstance().verifyIsProvided(video.getArticleId(), "Article id");
        Verifier.getInstance().verifyExists(articleRepository.getById(video.getArticleId()), "Article with id " + video.getArticleId());

        videoRepository.save(video);
    }

    @Override
    public void edit(Integer id, Video video) throws SQLException {
        Verifier.getInstance().verifyExists(videoRepository.getById(id), "Video with id " + id);

        if (hasTitle(video))
            Verifier.getInstance().verifyIsNotNullEmptyOrBlank(video.getTitle(), "Video title");
        if (hasUrl(video))
            Verifier.getInstance().verifyIsNotNullEmptyOrBlank(video.getUrl(), "Video url");
        if (hasText(video))
            Verifier.getInstance().verifyExists(textRepository.getById(video.getTextId()), "Text with id " + video.getTextId());
        if (hasArticle(video))
            Verifier.getInstance().verifyExists(articleRepository.getById(video.getArticleId()), "Article with id " + video.getArticleId());

        videoRepository.update(id, video);
    }

    @Override
    public void delete(Integer id) throws SQLException {
        Verifier.getInstance().verifyExists(videoRepository.getById(id), "Video with id " + id);

        videoRepository.delete(id);
    }

    @Override
    public List<VideoDTO> getForArticle(Integer id) throws SQLException {
        Verifier.getInstance().verifyExists(articleRepository.getById(id), "Article with id " + id);

        List<VideoDTO> videoDTOS = videoRepository.getForArticle(id);
        Verifier.getInstance().verifySystemHasAny(videoDTOS, "videos for article with id " + id);

        return videoDTOS;
    }

    private boolean hasTitle(Video video) {
        return video.getTitle() != null;
    }

    private boolean hasUrl(Video video) {
        return video.getUrl() != null;
    }

    private boolean hasText(Video video) {
        return video.getTextId() != null;
    }

    private boolean hasArticle(Video video) {
        return video.getArticleId() != null;
    }
}
