package com.example.school.services;

import com.example.school.models.Video;
import com.example.school.models.dtos.VideoDTO;

import java.sql.SQLException;
import java.util.List;

public interface VideoService {
    void create(Video video) throws SQLException;

    void edit(Integer id, Video video) throws SQLException;

    void delete(Integer id) throws SQLException;

    List<VideoDTO> getForArticle(Integer id) throws SQLException;
}
