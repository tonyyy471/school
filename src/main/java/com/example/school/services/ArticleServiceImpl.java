package com.example.school.services;

import com.example.school.models.Article;
import com.example.school.models.dtos.ArticleDTO;
import com.example.school.models.dtos.CategoryDTO;
import com.example.school.repositories.ArticleRepository;
import com.example.school.repositories.CategoryRepository;
import com.example.school.repositories.TextRepository;
import com.example.school.services.utils.Verifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {
    private final ArticleRepository articleRepository;
    private final CategoryRepository categoryRepository;
    private final TextRepository textRepository;

    @Autowired
    public ArticleServiceImpl(ArticleRepository articleRepository, CategoryRepository categoryRepository, TextRepository textRepository) {
        this.articleRepository = articleRepository;
        this.categoryRepository = categoryRepository;
        this.textRepository = textRepository;
    }

    @Override
    public void create(Article article) throws SQLException {
        Verifier.getInstance().verifyIsNotNullEmptyOrBlank(article.getTitle(), "Article title");

        Verifier.getInstance().verifyIsProvided(article.getCategoryId(), "Category id");
        Verifier.getInstance().verifyExists(categoryRepository.getById(article.getCategoryId()), "Category");

        if (hasText(article))
            Verifier.getInstance().verifyExists(textRepository.getById(article.getTextId()), "Text");

        articleRepository.save(article);
    }

    @Override
    public void edit(Integer id, Article article) throws SQLException {
        Verifier.getInstance().verifyExists(articleRepository.getById(id), "Article");

        if (hasTitle(article))
            Verifier.getInstance().verifyIsNotNullEmptyOrBlank(article.getTitle(), "Article title");

        if (hasCategory(article))
            Verifier.getInstance().verifyExists(categoryRepository.getById(article.getCategoryId()), "Category");

        if (hasText(article))
            Verifier.getInstance().verifyExists(textRepository.getById(article.getTextId()), "Text");

        articleRepository.update(id, article);
    }

    @Override
    public void delete(Integer id) throws SQLException {
        Verifier.getInstance().verifyExists(articleRepository.getById(id), "Article");

        articleRepository.delete(id);
    }

    @Override
    public List<ArticleDTO> getLastFive() throws SQLException {
        List<ArticleDTO> articleDTOS = articleRepository.getLastFive();
        Verifier.getInstance().verifySystemHasAny(articleDTOS, "articles");

        return articleDTOS;
    }

    @Override
    public List<ArticleDTO> getForCategory(Integer id) throws SQLException {
        CategoryDTO categoryDTO = categoryRepository.getById(id);
        Verifier.getInstance().verifyExists(categoryDTO, "Category");

        List<ArticleDTO> articleDTOS = articleRepository.getForCategory(id);
        Verifier.getInstance().verifySystemHasAny(articleDTOS, "articles for category" + categoryDTO.getName());

        return articleDTOS;
    }

    @Override
    public List<ArticleDTO> getForMonth(Integer year, Integer month) throws SQLException, ParseException {
        Verifier.getInstance().verifyRange(month, 1, 12, "Month");

        List<ArticleDTO> articleDTOS = articleRepository.getForMonth(year, month);
        Verifier.getInstance().verifySystemHasAny(articleDTOS, "articles for year " + year + " month " + month);

        return articleDTOS;
    }

    private boolean hasTitle(Article article) {
        return article.getTitle() != null;
    }

    private boolean hasCategory(Article article) {
        return article.getCategoryId() != null;
    }

    private boolean hasText(Article article) {
        return article.getTextId() != null;
    }
}
