package com.example.school.services;

import com.example.school.models.Category;
import com.example.school.models.dtos.CategoryDTO;
import com.example.school.repositories.CategoryRepository;
import com.example.school.services.utils.Verifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<CategoryDTO> getAll() throws SQLException {
        List<CategoryDTO> categoryDTOS = categoryRepository.getAll();
        Verifier.getInstance().verifySystemHasAny(categoryDTOS, "categories");

        return categoryDTOS;
    }

    @Override
    public void create(Category category) throws SQLException {
        Verifier.getInstance().verifyIsNotNullEmptyOrBlank(category.getName(), "Category name");

        CategoryDTO categoryDTO = categoryRepository.getByName(category);
        Verifier.getInstance().verifyDoesntExist(categoryDTO, "Category");

        categoryRepository.save(category);
    }

    @Override
    public void edit(Integer id, Category category) throws SQLException {
        Verifier.getInstance().verifyIsNotNullEmptyOrBlank(category.getName(), "Category name");

        CategoryDTO categoryDTO = categoryRepository.getById(id);
        Verifier.getInstance().verifyExists(categoryDTO, "Category");

        categoryRepository.update(id, category);
    }

    @Override
    public void delete(Integer id) throws SQLException {
        CategoryDTO categoryDTO = categoryRepository.getById(id);
        Verifier.getInstance().verifyExists(categoryDTO, "Category");

        categoryRepository.delete(id);
    }
}
