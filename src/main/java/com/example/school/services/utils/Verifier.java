package com.example.school.services.utils;

import com.example.school.exceptions.NoContentException;
import com.example.school.exceptions.NotAcceptableException;
import com.example.school.exceptions.NotExistingException;

import java.util.List;

public class Verifier {
    private static Verifier verifier;

    private Verifier() {
    }

    public static Verifier getInstance() {
        if (verifier == null)
            verifier = new Verifier();
        return verifier;
    }

    public void verifyIsProvided(Object parameter, String parameterName) {
        if (parameter == null)
            throw new NotAcceptableException(parameterName + " cannot be null");
    }

    public void verifyExists(Object object, String message) {
        if (object == null)
            throw new NotExistingException(message + " does not exist");
    }

    public void verifyDoesntExist(Object object, String message) {
        if (object != null)
            throw new NotAcceptableException(message + " already exists");
    }

    public void verifySystemHasAny(Object objects, String type) {
        if (!(objects instanceof List))
            throw new IllegalArgumentException("Illegal first parameter type. Expected List, but was " + objects.getClass().getSimpleName());
        if (((List) objects).isEmpty())
            throw new NoContentException("There are no " + type + " in the system");
    }

    public void verifyIsNotNullEmptyOrBlank(String parameter, String parameterName) {
        if (parameter == null || parameter.isEmpty() || parameter.isBlank())
            throw new NotAcceptableException(parameterName + " cannot be empty or blank");
    }

    public void verifyRange(Integer parameter, int lowest, int highest, String parameterName) {
        if (parameter < lowest || parameter > highest)
            throw new NotAcceptableException(parameterName + " cannot be less than " + lowest + " or more than " + highest);
    }
}
