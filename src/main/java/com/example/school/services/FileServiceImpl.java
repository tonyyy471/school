package com.example.school.services;

import com.example.school.exceptions.FileException;
import com.example.school.exceptions.NotExistingException;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.Objects;

@Service
public class FileServiceImpl implements FileService {
    private Path fileLocation;

    public FileServiceImpl() {
        instantiateFileLocation();
        createDirectoryIfDoesntExist();
    }

    @Override
    public String store(MultipartFile file) {
        String fileName = dateAsMillis() + StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        try {
            if (fileName.contains("..")) throw new FileException("File name contains illegal characters");

            Path targetLocation = this.fileLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            throw new FileException("Error storing file");
        }
        return fileName;
    }

    @Override
    public byte[] getFileBytesForName(String fileName) {
        try {
            File file = loadFileAsResource(fileName);
            InputStream inputStream = new FileInputStream(Objects.requireNonNull(file));
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            writeData(buffer, inputStream);

            return buffer.toByteArray();
        } catch (IOException ex) {
            return null;
        }
    }

    private File loadFileAsResource(String fileName) {
        try {
            Path directoryPath = Paths.get("files").toAbsolutePath().normalize();
            Path filePath = directoryPath.resolve(fileName).normalize();

            Resource resource = new UrlResource(filePath.toUri());

            if (resource.exists()) return resource.getFile();
            else throw new NotExistingException("File with name " + fileName + " does not exist");
        } catch (IOException ex) {
            throw new NotExistingException("File with name " + fileName + " does not exist");
        }
    }

    private void writeData(ByteArrayOutputStream buffer, InputStream inputStream) throws IOException {
        int data = inputStream.read();
        while (data != -1) {
            buffer.write(data);
            data = inputStream.read();
        }
        inputStream.close();
    }

    private void instantiateFileLocation() {
        this.fileLocation = Paths.get("files").toAbsolutePath().normalize();
    }

    private void createDirectoryIfDoesntExist() {
        try {
            Files.createDirectories(this.fileLocation);
        } catch (Exception ex) {
            throw new FileException("Directory not created");
        }
    }

    private String dateAsMillis() {
        return new Date().getTime() + " ";
    }
}
