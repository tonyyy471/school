package com.example.school.services;

import com.example.school.models.Document;
import com.example.school.models.dtos.DocumentDTO;

import java.sql.SQLException;
import java.util.List;

public interface DocumentService {
    void create(Document document) throws SQLException;

    void edit(Integer id, Document document) throws SQLException;

    void delete(Integer id) throws SQLException;

    List<DocumentDTO> getForArticle(Integer id) throws SQLException;
}
