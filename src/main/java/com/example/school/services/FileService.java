package com.example.school.services;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    String store(MultipartFile file);

    byte[] getFileBytesForName(String fileName);
}
