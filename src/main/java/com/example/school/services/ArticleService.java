package com.example.school.services;

import com.example.school.models.Article;
import com.example.school.models.dtos.ArticleDTO;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface ArticleService {
    void create(Article article) throws SQLException;

    void edit(Integer id, Article article) throws SQLException;

    void delete(Integer id) throws SQLException;

    List<ArticleDTO> getLastFive() throws SQLException;

    List<ArticleDTO> getForCategory(Integer id) throws SQLException;

    List<ArticleDTO> getForMonth(Integer year, Integer month) throws SQLException, ParseException;
}
