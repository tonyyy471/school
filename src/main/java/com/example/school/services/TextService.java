package com.example.school.services;

import com.example.school.models.Text;
import com.example.school.models.dtos.TextDTO;

import java.sql.SQLException;

public interface TextService {
    int create(Text text) throws SQLException;

    void edit(Integer id, Text text) throws SQLException;

    TextDTO getById(Integer id) throws SQLException;

    void delete(Integer id) throws SQLException;
}
