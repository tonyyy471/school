package com.example.school.services;

import com.example.school.models.Text;
import com.example.school.models.dtos.TextDTO;
import com.example.school.repositories.TextRepository;
import com.example.school.services.utils.Verifier;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

@Service
public class TextServiceImpl implements TextService {
    private final TextRepository textRepository;

    public TextServiceImpl(TextRepository textRepository) {
        this.textRepository = textRepository;
    }

    @Override
    public int create(Text text) throws SQLException {
        Verifier.getInstance().verifyIsNotNullEmptyOrBlank(text.getContent(), "Text content");
        return textRepository.save(text);
    }

    @Override
    public void edit(Integer id, Text text) throws SQLException {
        Verifier.getInstance().verifyIsNotNullEmptyOrBlank(text.getContent(), "Text content");

        TextDTO textDTO = textRepository.getById(id);
        Verifier.getInstance().verifyExists(textDTO, "Text");

        textRepository.update(id, text);
    }

    @Override
    public TextDTO getById(Integer id) throws SQLException {
        TextDTO textDTO = textRepository.getById(id);
        Verifier.getInstance().verifyExists(textDTO, "Text");

        return textDTO;
    }

    @Override
    public void delete(Integer id) throws SQLException {
        TextDTO textDTO = textRepository.getById(id);
        Verifier.getInstance().verifyExists(textDTO, "Text");

        textRepository.delete(id);
    }
}
