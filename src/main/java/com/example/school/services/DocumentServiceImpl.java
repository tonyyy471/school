package com.example.school.services;

import com.example.school.models.Document;
import com.example.school.models.dtos.DocumentDTO;
import com.example.school.repositories.ArticleRepository;
import com.example.school.repositories.DocumentRepository;
import com.example.school.repositories.TextRepository;
import com.example.school.services.utils.Verifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {
    private final DocumentRepository documentRepository;
    private final FileService fileService;
    private final TextRepository textRepository;
    private final ArticleRepository articleRepository;

    @Autowired
    public DocumentServiceImpl(DocumentRepository documentRepository, FileService fileService, TextRepository textRepository, ArticleRepository articleRepository) {
        this.documentRepository = documentRepository;
        this.fileService = fileService;
        this.textRepository = textRepository;
        this.articleRepository = articleRepository;
    }

    @Override
    public void create(Document document) throws SQLException {
        Verifier.getInstance().verifyIsNotNullEmptyOrBlank(document.getTitle(), "Document title");

        Verifier.getInstance().verifyIsProvided(document.getFileName(), "File name");
        Verifier.getInstance().verifyExists(fileService.getFileBytesForName(document.getFileName()), "File with name " + document.getFileName());

        if (hasText(document))
            Verifier.getInstance().verifyExists(textRepository.getById(document.getTextId()), "Text with id " + document.getTextId());

        Verifier.getInstance().verifyIsProvided(document.getArticleId(), "Article id");
        Verifier.getInstance().verifyExists(articleRepository.getById(document.getArticleId()), "Article with id " + document.getArticleId());

        documentRepository.save(document);
    }

    @Override
    public void edit(Integer id, Document document) throws SQLException {
        Verifier.getInstance().verifyExists(documentRepository.getById(id), "Document with id " + id);

        if (hasTitle(document))
            Verifier.getInstance().verifyIsNotNullEmptyOrBlank(document.getTitle(), "Document title");
        if (hasFile(document))
            Verifier.getInstance().verifyExists(fileService.getFileBytesForName(document.getFileName()), "File with name " + document.getFileName());
        if (hasText(document))
            Verifier.getInstance().verifyExists(textRepository.getById(document.getTextId()), "Text with id " + document.getTextId());
        if (hasArticle(document))
            Verifier.getInstance().verifyExists(articleRepository.getById(document.getArticleId()), "Article with id " + document.getArticleId());

        documentRepository.update(id, document);
    }

    @Override
    public void delete(Integer id) throws SQLException {
        Verifier.getInstance().verifyExists(documentRepository.getById(id), "Document with id " + id);

        documentRepository.delete(id);
    }

    @Override
    public List<DocumentDTO> getForArticle(Integer id) throws SQLException {
        Verifier.getInstance().verifyExists(articleRepository.getById(id), "Article with id " + id);

        List<DocumentDTO> documentDTOS = documentRepository.getForArticle(id);
        Verifier.getInstance().verifySystemHasAny(documentDTOS, "documents for article with id " + id);

        documentDTOS.forEach(documentDTO -> documentDTO.setFileBytes(fileService.getFileBytesForName(documentDTO.getFileName())));

        return documentDTOS;
    }

    private boolean hasTitle(Document document) {
        return document.getTitle() != null;
    }

    private boolean hasFile(Document document) {
        return document.getFileName() != null;
    }

    private boolean hasText(Document document) {
        return document.getTextId() != null;
    }

    private boolean hasArticle(Document document) {
        return document.getArticleId() != null;
    }
}
