package com.example.school.services;

import com.example.school.models.Login;
import com.example.school.models.dtos.JWTokenDTO;

public interface UserService {
    JWTokenDTO login(Login login);
}
