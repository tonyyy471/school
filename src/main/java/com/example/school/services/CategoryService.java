package com.example.school.services;

import com.example.school.models.Category;
import com.example.school.models.dtos.CategoryDTO;

import java.sql.SQLException;
import java.util.List;

public interface CategoryService {
    List<CategoryDTO> getAll() throws SQLException;

    void create(Category category) throws SQLException;

    void edit(Integer id, Category category) throws SQLException;

    void delete(Integer id) throws SQLException;
}
