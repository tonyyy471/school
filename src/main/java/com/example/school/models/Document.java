package com.example.school.models;

public class Document {
    private String title;
    private String fileName;
    private Integer textId;
    private Integer articleId;

    public Document() {
    }

    public Document(String title, String fileName, Integer textId, Integer articleId) {
        this.title = title;
        this.fileName = fileName;
        this.textId = textId;
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getTextId() {
        return textId;
    }

    public void setTextId(Integer textId) {
        this.textId = textId;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }
}
