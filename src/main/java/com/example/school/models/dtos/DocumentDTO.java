package com.example.school.models.dtos;

public class DocumentDTO {
    private final int id;
    private final String title;
    private final String fileName;
    private  byte[] fileBytes;
    private final Integer textId;
    private final Integer articleId;

    public DocumentDTO(int id, String title, String fileName, byte[] fileBytes, Integer textId, Integer articleId) {
        this.id = id;
        this.title = title;
        this.fileName = fileName;
        this.fileBytes = fileBytes;
        this.textId = textId;
        this.articleId = articleId;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getFileName() {
        return fileName;
    }

    public byte[] getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }

    public Integer getTextId() {
        return textId;
    }

    public Integer getArticleId() {
        return articleId;
    }
}
