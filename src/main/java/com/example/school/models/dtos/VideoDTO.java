package com.example.school.models.dtos;

public class VideoDTO {
    private final int id;
    private final String title;
    private final String url;
    private final Integer textId;
    private final Integer articleId;

    public VideoDTO(int id, String title, String url, Integer textId, Integer articleId) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.textId = textId;
        this.articleId = articleId;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public Integer getTextId() {
        return textId;
    }

    public Integer getArticleId() {
        return articleId;
    }
}
