package com.example.school.models.dtos;

public class TextDTO {
    private final int id;
    private final String content;

    public TextDTO(int id, String content) {
        this.id = id;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
