package com.example.school.models.dtos;

public class JWTokenDTO {
    private final String token;

    public JWTokenDTO(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
