package com.example.school.models.dtos;

public class CategoryDTO {
    private final int id;
    private final String name;

    public CategoryDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
