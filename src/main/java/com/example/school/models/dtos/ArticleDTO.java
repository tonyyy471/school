package com.example.school.models.dtos;

public class ArticleDTO {
    private final int id;
    private final String timestamp;
    private final String title;
    private final Integer categoryId;
    private final Integer textId;

    public ArticleDTO(int id, String timestamp, String title, Integer categoryId, Integer textId) {
        this.id = id;
        this.timestamp = timestamp;
        this.title = title;
        this.categoryId = categoryId;
        this.textId = textId;
    }

    public int getId() {
        return id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getTitle() {
        return title;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Integer getTextId() {
        return textId;
    }
}
