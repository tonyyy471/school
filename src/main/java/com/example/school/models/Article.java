package com.example.school.models;

public class Article {
    private  String title;
    private  Integer categoryId;
    private  Integer textId;

    public Article() {
    }

    public Article(String title, Integer categoryId, Integer textId) {
        this.title = title;
        this.categoryId = categoryId;
        this.textId = textId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getTextId() {
        return textId;
    }

    public void setTextId(Integer textId) {
        this.textId = textId;
    }
}
