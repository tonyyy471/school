package com.example.school.models;

public class Video {
    private String title;
    private String url;
    private Integer textId;
    private Integer articleId;

    public Video() {
    }

    public Video(String title, String url, Integer textId, Integer articleId) {
        this.title = title;
        this.url = url;
        this.textId = textId;
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getTextId() {
        return textId;
    }

    public void setTextId(Integer textId) {
        this.textId = textId;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }
}
