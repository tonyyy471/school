package com.example.school.models.utils;

import com.example.school.models.dtos.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ModelMapper {
    private static ModelMapper modelMapper;

    private ModelMapper() {
    }

    public static ModelMapper getInstance() {
        if (modelMapper == null)
            modelMapper = new ModelMapper();
        return modelMapper;
    }

    public TextDTO toTextDTO(ResultSet resultSet) throws SQLException {
        return new TextDTO(resultSet.getInt("id"), resultSet.getString("content"));
    }

    public List<CategoryDTO> toCategoriesDTO(ResultSet resultSet) throws SQLException {
        List<CategoryDTO> categoryDTOS = new ArrayList<>();
        while (resultSet.next())
            categoryDTOS.add(toCategoryDTO(resultSet));
        return categoryDTOS;
    }

    public CategoryDTO toCategoryDTO(ResultSet resultSet) throws SQLException {
        return new CategoryDTO(resultSet.getInt("id"), resultSet.getString("name"));
    }

    public List<ArticleDTO> toArticlesDTO(ResultSet resultSet) throws SQLException {
        List<ArticleDTO> articleDTOS = new ArrayList<>();
        while (resultSet.next())
            articleDTOS.add(toArticleDTO(resultSet));
        return articleDTOS;
    }

    public ArticleDTO toArticleDTO(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String timestamp = resultSet.getTimestamp("timestamp").toString();
        String title = resultSet.getString("title");
        Integer categoryId = resultSet.getInt("category_id");
        Integer textId = resultSet.getInt("text_id");
        textId = resultSet.wasNull() ? null : textId;

        return new ArticleDTO(id, timestamp, title, categoryId, textId);
    }

    public ImageDTO toImageDTO(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String title = resultSet.getString("title");
        title = resultSet.wasNull() ? null : title;
        String fileName = resultSet.getString("file_name");
        Integer textId = resultSet.getInt("text_id");
        textId = resultSet.wasNull() ? null : textId;
        int articleId = resultSet.getInt("article_id");

        return new ImageDTO(id, title, fileName, null, textId, articleId);
    }

    public List<ImageDTO> toImagesDTO(ResultSet resultSet) throws SQLException {
        List<ImageDTO> imageDTOS = new ArrayList<>();
        while (resultSet.next())
            imageDTOS.add(toImageDTO(resultSet));
        return imageDTOS;
    }

    public DocumentDTO toDocumentDTO(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String title = resultSet.getString("title");
        String fileName = resultSet.getString("file_name");
        Integer textId = resultSet.getInt("text_id");
        textId = resultSet.wasNull() ? null : textId;
        int articleId = resultSet.getInt("article_id");

        return new DocumentDTO(id, title, fileName, null, textId, articleId);
    }

    public List<DocumentDTO> toDocumentsDTO(ResultSet resultSet) throws SQLException {
        List<DocumentDTO> documentDTOS = new ArrayList<>();
        while (resultSet.next())
            documentDTOS.add(toDocumentDTO(resultSet));
        return documentDTOS;
    }

    public VideoDTO toVideoDTO(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String title = resultSet.getString("title");
        title = resultSet.wasNull() ? null : title;
        String url = resultSet.getString("url");
        Integer textId = resultSet.getInt("text_id");
        textId = resultSet.wasNull() ? null : textId;
        int articleId = resultSet.getInt("article_id");

        return new VideoDTO(id, title, url, textId, articleId);
    }

    public List<VideoDTO> toVideosDTO(ResultSet resultSet) throws SQLException {
        List<VideoDTO> videoDTOS = new ArrayList<>();
        while (resultSet.next())
            videoDTOS.add(toVideoDTO(resultSet));
        return videoDTOS;
    }

    public UserDTO toUserDTO(ResultSet resultSet) throws SQLException {
        return new UserDTO(resultSet.getInt("id"),
                resultSet.getString("username"), resultSet.getString("password"));
    }
}
