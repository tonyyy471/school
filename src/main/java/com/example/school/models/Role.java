package com.example.school.models;

import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;

public enum Role implements GrantedAuthority {
    ROLE_ADMIN;

    @Override
    public String getAuthority() {
        return name();
    }

    public static List<Role> provideRoles(String username) {
        List<Role> roles = new ArrayList<>();
        roles.add(ROLE_ADMIN);
        return roles;
    }
}
