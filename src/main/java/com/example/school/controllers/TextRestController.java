package com.example.school.controllers;

import com.example.school.models.Text;
import com.example.school.models.dtos.TextDTO;
import com.example.school.services.TextService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@RestController
@RequestMapping("/api/texts")
public class TextRestController {
    private final TextService textService;

    public TextRestController(TextService textService) {
        this.textService = textService;
    }

    @PostMapping("/create")
    public ResponseEntity<Integer> create(@RequestBody Text text) throws SQLException {
        int id = textService.create(text);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PutMapping("/edit")
    public void edit(@RequestParam Integer id, @RequestBody Text text) throws SQLException {
        textService.edit(id, text);
    }

    @GetMapping("/get")
    public ResponseEntity<TextDTO> get(@RequestParam Integer id) throws SQLException {
        TextDTO textDTO = textService.getById(id);
        return new ResponseEntity<>(textDTO, HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam Integer id)throws SQLException {
        textService.delete(id);
    }
}
