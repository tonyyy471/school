package com.example.school.controllers;

import com.example.school.models.Document;
import com.example.school.models.Image;
import com.example.school.models.dtos.DocumentDTO;
import com.example.school.models.dtos.ImageDTO;
import com.example.school.services.DocumentService;
import com.example.school.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api/documents")
public class DocumentRestController {
    private final DocumentService documentService;

    @Autowired
    public DocumentRestController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @PostMapping("/create")
    public void create(@RequestBody Document document) throws SQLException {
        documentService.create(document);
    }

    @PutMapping("/edit")
    public void edit(@RequestParam Integer id, @RequestBody Document document) throws SQLException {
        documentService.edit(id, document);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam Integer id) throws SQLException {
        documentService.delete(id);
    }

    @GetMapping("/get-for-article")
    public ResponseEntity<List<DocumentDTO>> getForArticle(@RequestParam Integer id) throws SQLException {
        List<DocumentDTO> documentDTOS = documentService.getForArticle(id);
        return new ResponseEntity<>(documentDTOS, HttpStatus.OK);
    }
}
