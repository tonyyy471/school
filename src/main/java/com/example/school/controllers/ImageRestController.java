package com.example.school.controllers;

import com.example.school.models.Image;
import com.example.school.models.dtos.ImageDTO;
import com.example.school.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api/images")
public class ImageRestController {
    private final ImageService imageService;

    @Autowired
    public ImageRestController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping("/create")
    public void create(@RequestBody Image image) throws SQLException {
        imageService.create(image);
    }

    @PutMapping("/edit")
    public void edit(@RequestParam Integer id, @RequestBody Image image) throws SQLException {
        imageService.edit(id, image);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam Integer id) throws SQLException {
        imageService.delete(id);
    }

    @GetMapping("/get-for-article")
    public ResponseEntity<List<ImageDTO>> getForArticle(@RequestParam Integer id) throws SQLException {
        List<ImageDTO> imageDTOS = imageService.getForArticle(id);
        return new ResponseEntity<>(imageDTOS, HttpStatus.OK);
    }
}
