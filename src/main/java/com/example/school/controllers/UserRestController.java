package com.example.school.controllers;

import com.example.school.models.Login;
import com.example.school.models.dtos.JWTokenDTO;
import com.example.school.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private final UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<JWTokenDTO> login(@RequestBody Login login) {
        JWTokenDTO jwtToken = userService.login(login);
        return new ResponseEntity<>(jwtToken, HttpStatus.OK);
    }
}
