package com.example.school.controllers;

import com.example.school.models.Category;
import com.example.school.models.dtos.CategoryDTO;
import com.example.school.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategoryRestController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryRestController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<CategoryDTO>> getAll() throws SQLException {
        List<CategoryDTO> categoryDTOs = categoryService.getAll();
        return new ResponseEntity<>(categoryDTOs, HttpStatus.OK);
    }

    @PostMapping("/create")
    public void create(@RequestBody Category category) throws SQLException {
        categoryService.create(category);
    }

    @PutMapping("/edit")
    public void edit(@RequestParam Integer id, @RequestBody Category category) throws SQLException {
        categoryService.edit(id, category);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam Integer id) throws SQLException{
        categoryService.delete(id);
    }
}
