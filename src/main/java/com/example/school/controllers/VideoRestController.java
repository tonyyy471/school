package com.example.school.controllers;

import com.example.school.models.Video;
import com.example.school.models.dtos.VideoDTO;
import com.example.school.services.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api/videos")
public class VideoRestController {
    private final VideoService videoService;

    @Autowired
    public VideoRestController(VideoService videoService) {
        this.videoService = videoService;
    }

    @PostMapping("/create")
    public void create(@RequestBody Video video) throws SQLException {
        videoService.create(video);
    }

    @PutMapping("/edit")
    public void edit(@RequestParam Integer id, @RequestBody Video video) throws SQLException {
        videoService.edit(id, video);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam Integer id) throws SQLException {
        videoService.delete(id);
    }

    @GetMapping("get-for-article")
    public ResponseEntity<List<VideoDTO>> getForArticle(@RequestParam Integer id) throws SQLException {
        List<VideoDTO> videoDTOS = videoService.getForArticle(id);
        return new ResponseEntity<>(videoDTOS, HttpStatus.OK);
    }
}
