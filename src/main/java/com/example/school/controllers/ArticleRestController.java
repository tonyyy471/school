package com.example.school.controllers;

import com.example.school.models.Article;
import com.example.school.models.dtos.ArticleDTO;
import com.example.school.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/api/articles")
public class ArticleRestController {
    private final ArticleService articleService;

    @Autowired
    public ArticleRestController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @PostMapping("/create")
    public void create(@RequestBody Article article) throws SQLException {
        articleService.create(article);
    }

    @PutMapping("/edit")
    public void edit(@RequestParam Integer id, @RequestBody Article article) throws SQLException {
        articleService.edit(id, article);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestParam Integer id) throws SQLException {
        articleService.delete(id);
    }

    @GetMapping("/get-last-five")
    public ResponseEntity<List<ArticleDTO>> getLastFive() throws SQLException {
        List<ArticleDTO> articleDTOS = articleService.getLastFive();
        return new ResponseEntity<>(articleDTOS, HttpStatus.OK);
    }

    @GetMapping("/get-for-category")
    public ResponseEntity<List<ArticleDTO>> getForCategory(Integer id) throws SQLException {
        List<ArticleDTO> articleDTOS = articleService.getForCategory(id);
        return new ResponseEntity<>(articleDTOS, HttpStatus.OK);
    }

    @GetMapping("/get-for-month")
    public ResponseEntity<List<ArticleDTO>> getForMonth(Integer year, Integer month) throws SQLException, ParseException {
        List<ArticleDTO> articleDTOS = articleService.getForMonth(year, month);
        return new ResponseEntity<>(articleDTOS, HttpStatus.OK);
    }
}
