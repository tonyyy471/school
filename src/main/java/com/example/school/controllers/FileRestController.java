package com.example.school.controllers;

import com.example.school.services.FileService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/files")
public class FileRestController {
    private final FileService fileService;

    public FileRestController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping
    public ResponseEntity<String> upload(@RequestParam(name = "file") MultipartFile file) {
        String fileLocation = fileService.store(file);
        return new ResponseEntity<>(fileLocation, HttpStatus.OK);
    }
}
