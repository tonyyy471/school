package com.example.school.repositories;

import com.example.school.models.Text;
import com.example.school.models.dtos.TextDTO;

import java.sql.SQLException;

public interface TextRepository {
    int save(Text text) throws SQLException;

    void update(Integer id, Text text) throws SQLException;

    TextDTO getById(Integer id) throws SQLException;

    void delete(Integer id) throws SQLException;
}
