package com.example.school.repositories;

import com.example.school.models.Document;
import com.example.school.models.dtos.DocumentDTO;
import com.example.school.models.utils.ModelMapper;
import com.example.school.repositories.utils.ConnectionWrapper;
import com.example.school.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class DocumentRepositoryImpl implements DocumentRepository {
    @Override
    public void save(Document document) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().saveDocumentQuery(hasText(document)))) {

            int parameterIndex = 1;

            statement.setString(parameterIndex++, document.getTitle());
            statement.setString(parameterIndex++, document.getFileName());
            if (hasText(document)) statement.setInt(parameterIndex++, document.getTextId());
            statement.setInt(parameterIndex, document.getArticleId());
            statement.executeUpdate();
        }
    }

    @Override
    public DocumentDTO getById(Integer id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getDocumentByIdQuery())) {

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toDocumentDTO(resultSet) : null;
    }

    @Override
    public void update(Integer id, Document document) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance()
                        .updateDocumentQuery(hasTitle(document), hasFile(document), hasText(document), hasArticle(document)))) {

            int parameterIndex = 1;

            if (hasTitle(document)) statement.setString(parameterIndex++, document.getTitle());
            if (hasFile(document)) statement.setString(parameterIndex++, document.getFileName());
            if (hasText(document)) statement.setInt(parameterIndex++, document.getTextId());
            if (hasArticle(document)) statement.setInt(parameterIndex++, document.getArticleId());

            if (hasAnythingToUpdate(document)) {
                statement.setInt(parameterIndex, id);
                statement.executeUpdate();
            }
        }
    }

    @Override
    public void delete(Integer id) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().deleteDocumentQuery())) {

            statement.setInt(1, id);
            statement.executeUpdate();
        }
    }

    @Override
    public List<DocumentDTO> getForArticle(Integer id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getDocumentsForArticle())) {

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toDocumentsDTO(resultSet);
    }

    private boolean hasTitle(Document document) {
        return document.getTitle() != null;
    }

    private boolean hasFile(Document document) {
        return document.getFileName() != null;
    }

    private boolean hasText(Document document) {
        return document.getTextId() != null;
    }

    private boolean hasArticle(Document document) {
        return document.getArticleId() != null;
    }

    private boolean hasAnythingToUpdate(Document document) {
        return hasTitle(document) || hasFile(document) || hasText(document) || hasArticle(document);
    }
}
