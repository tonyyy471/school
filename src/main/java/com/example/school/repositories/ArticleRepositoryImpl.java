package com.example.school.repositories;

import com.example.school.models.Article;
import com.example.school.models.dtos.ArticleDTO;
import com.example.school.models.utils.ModelMapper;
import com.example.school.repositories.utils.ConnectionWrapper;
import com.example.school.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class ArticleRepositoryImpl implements ArticleRepository {

    @Override
    public void save(Article article) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().saveArticleQuery(hasText(article)))) {

            statement.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            statement.setString(2, article.getTitle());
            statement.setInt(3, article.getCategoryId());
            if (hasText(article)) statement.setInt(4, article.getTextId());

            statement.executeUpdate();
        }
    }

    @Override
    public ArticleDTO getById(Integer id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getArticleByIdQuery())) {

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toArticleDTO(resultSet) : null;
    }

    @Override
    public void update(Integer id, Article article) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().updateArticleQuery(hasTitle(article), hasCategory(article), hasText(article)))) {

            int parameterIndex = 1;

            if (hasTitle(article)) statement.setString(parameterIndex++, article.getTitle());
            if (hasCategory(article)) statement.setInt(parameterIndex++, article.getCategoryId());
            if (hasText(article)) statement.setInt(parameterIndex++, article.getTextId());

            if (hasAnythingToUpdate(article)) {
                statement.setInt(parameterIndex, id);
                statement.executeUpdate();
            }
        }
    }

    @Override
    public void delete(Integer id) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().deleteArticleQuery())) {

            statement.setInt(1, id);
            statement.executeUpdate();
        }
    }

    @Override
    public List<ArticleDTO> getLastFive() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getLastFiveArticlesQuery())) {

            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toArticlesDTO(resultSet);
    }

    @Override
    public List<ArticleDTO> getForCategory(Integer id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getArticlesForCategoryQuery())) {

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toArticlesDTO(resultSet);
    }

    @Override
    public List<ArticleDTO> getForMonth(Integer year, Integer month) throws SQLException, ParseException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getArticlesForMonthQuery())) {

            statement.setTimestamp(1, new Timestamp(getMillis(year, month)));

            if (month == 12) {
                year++;
                month = 1;
            } else month++;

            statement.setTimestamp(2, new Timestamp(getMillis(year, month)));

            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toArticlesDTO(resultSet);
    }

    private long getMillis(Integer year, Integer month) throws ParseException {
        String monthAsString = month < 10 ? "/0" + month : "/" + month;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date date = sdf.parse(year + monthAsString + "/01 " + "00:00:00");
        return date.getTime();
    }

    private boolean hasTitle(Article article) {
        return article.getTitle() != null;
    }

    private boolean hasCategory(Article article) {
        return article.getCategoryId() != null;
    }

    private boolean hasText(Article article) {
        return article.getTextId() != null;
    }

    private boolean hasAnythingToUpdate(Article article) {
        return hasTitle(article) || hasCategory(article) || hasText(article);
    }
}
