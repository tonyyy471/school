package com.example.school.repositories;

import com.example.school.models.Document;
import com.example.school.models.dtos.DocumentDTO;

import java.sql.SQLException;
import java.util.List;

public interface DocumentRepository {
    void save(Document document) throws SQLException;

    DocumentDTO getById(Integer id) throws SQLException;

    void update(Integer id, Document document) throws SQLException;

    void delete(Integer id) throws SQLException;

    List<DocumentDTO> getForArticle(Integer id) throws SQLException;
}
