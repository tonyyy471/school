package com.example.school.repositories;

import com.example.school.models.Image;
import com.example.school.models.dtos.ImageDTO;

import java.sql.SQLException;
import java.util.List;

public interface ImageRepository {
    void save(Image image) throws SQLException;

    void update(Integer id, Image image) throws SQLException;

    ImageDTO getById(Integer id) throws SQLException;

    void delete(Integer id) throws SQLException;

    List<ImageDTO> getForArticle(Integer id) throws SQLException;
}
