package com.example.school.repositories;

import com.example.school.models.Video;
import com.example.school.models.dtos.VideoDTO;

import java.sql.SQLException;
import java.util.List;

public interface VideoRepository {
    void save(Video video) throws SQLException;

    Object getById(Integer id) throws SQLException;

    void update(Integer id, Video video) throws SQLException;

    void delete(Integer id) throws SQLException;

    List<VideoDTO> getForArticle(Integer id) throws SQLException;
}
