package com.example.school.repositories;

import com.example.school.models.Image;
import com.example.school.models.dtos.ImageDTO;
import com.example.school.models.utils.ModelMapper;
import com.example.school.repositories.utils.ConnectionWrapper;
import com.example.school.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ImageRepositoryImpl implements ImageRepository {

    @Override
    public void save(Image image) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().saveImageQuery(hasTitle(image), hasText(image)))) {

            int parameterIndex = 1;

            if (hasTitle(image)) statement.setString(parameterIndex++, image.getTitle());
            statement.setString(parameterIndex++, image.getFileName());
            if (hasText(image)) statement.setInt(parameterIndex++, image.getTextId());
            statement.setInt(parameterIndex, image.getArticleId());
            statement.executeUpdate();
        }
    }

    @Override
    public void update(Integer id, Image image) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance()
                        .updateImageQuery(hasTitle(image), hasFile(image), hasText(image), hasArticle(image)))) {

            int parameterIndex = 1;

            if (hasTitle(image)) statement.setString(parameterIndex++, image.getTitle());
            if (hasFile(image)) statement.setString(parameterIndex++, image.getFileName());
            if (hasText(image)) statement.setInt(parameterIndex++, image.getTextId());
            if (hasArticle(image)) statement.setInt(parameterIndex++, image.getArticleId());

            if (hasAnythingToUpdate(image)) {
                statement.setInt(parameterIndex, id);
                statement.executeUpdate();
            }
        }
    }

    @Override
    public ImageDTO getById(Integer id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getImageByIdQuery())) {

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toImageDTO(resultSet) : null;
    }

    @Override
    public void delete(Integer id) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().deleteImageQuery())) {

            statement.setInt(1, id);
            statement.executeUpdate();
        }
    }

    @Override
    public List<ImageDTO> getForArticle(Integer id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getImagesForArticle())) {

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toImagesDTO(resultSet);
    }

    private boolean hasTitle(Image image) {
        return image.getTitle() != null;
    }

    private boolean hasFile(Image image) {
        return image.getFileName() != null;
    }

    private boolean hasText(Image image) {
        return image.getTextId() != null;
    }

    private boolean hasArticle(Image image) {
        return image.getArticleId() != null;
    }

    private boolean hasAnythingToUpdate(Image image) {
        return hasTitle(image) || hasFile(image) || hasText(image) || hasArticle(image);
    }
}
