package com.example.school.repositories;

import com.example.school.models.Article;
import com.example.school.models.dtos.ArticleDTO;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface ArticleRepository {
    void save(Article article) throws SQLException;

    ArticleDTO getById(Integer id) throws SQLException;

    void update(Integer id, Article article) throws SQLException;

    void delete(Integer id) throws SQLException;

    List<ArticleDTO> getLastFive() throws SQLException;

    List<ArticleDTO> getForCategory(Integer id) throws SQLException;

    List<ArticleDTO> getForMonth(Integer year, Integer month) throws SQLException, ParseException;
}
