package com.example.school.repositories;

import com.example.school.models.Text;
import com.example.school.models.dtos.TextDTO;
import com.example.school.models.utils.ModelMapper;
import com.example.school.repositories.utils.ConnectionWrapper;
import com.example.school.repositories.utils.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class TextRepositoryImpl implements TextRepository {
    private PasswordEncoder passwordEncoder;

    @Autowired
    public TextRepositoryImpl(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public int save(Text text) throws SQLException {
        int id;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().saveTextQuery(), Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, text.getContent());
            statement.executeUpdate();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            id = generatedKeys.getInt(1);
        }
        return id;
    }

    @Override
    public void update(Integer id, Text text) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().updateTextQuery())) {

            statement.setString(1, text.getContent());
            statement.setInt(2, id);
            statement.executeUpdate();
        }
    }

    @Override
    public TextDTO getById(Integer id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getTextQuery())) {

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toTextDTO(resultSet) : null;
    }

    @Override
    public void delete(Integer id) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().deleteTextQuery())) {

            statement.setInt(1, id);
            statement.executeUpdate();
        }
    }
}
