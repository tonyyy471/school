package com.example.school.repositories;

import com.example.school.models.Category;
import com.example.school.models.dtos.CategoryDTO;
import com.example.school.models.utils.ModelMapper;
import com.example.school.repositories.utils.ConnectionWrapper;
import com.example.school.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    @Override
    public List<CategoryDTO> getAll() throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getCategoriesQuery())) {

            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toCategoriesDTO(resultSet);
    }

    @Override
    public CategoryDTO getByName(Category category) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getCategoryByNameQuery())) {

            statement.setString(1, category.getName());
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toCategoryDTO(resultSet) : null;
    }

    @Override
    public void save(Category category) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().saveCategoryQuery())) {

            statement.setString(1, category.getName());
            statement.executeUpdate();
        }
    }

    @Override
    public CategoryDTO getById(Integer id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getCategoryByIdQuery())) {

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toCategoryDTO(resultSet) : null;
    }

    @Override
    public void update(Integer id, Category category) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().updateCategoryQuery())) {

            statement.setString(1, category.getName());
            statement.setInt(2, id);
            statement.executeUpdate();
        }
    }

    @Override
    public void delete(Integer id) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().deleteCategoryQuery())) {

            statement.setInt(1, id);
            statement.executeUpdate();
        }
    }
}
