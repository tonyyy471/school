package com.example.school.repositories;

import com.example.school.models.Video;
import com.example.school.models.dtos.VideoDTO;
import com.example.school.models.utils.ModelMapper;
import com.example.school.repositories.utils.ConnectionWrapper;
import com.example.school.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Service
public class VideoRepositoryImpl implements VideoRepository {

    @Override
    public void save(Video video) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().saveVideoQuery(hasTitle(video), hasText(video)))) {

            int parameterIndex = 1;

            if (hasTitle(video)) statement.setString(parameterIndex++, video.getTitle());
            statement.setString(parameterIndex++, video.getUrl());
            if (hasText(video)) statement.setInt(parameterIndex++, video.getTextId());
            statement.setInt(parameterIndex, video.getArticleId());
            statement.executeUpdate();
        }
    }

    @Override
    public Object getById(Integer id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getVideoByIdQuery())) {

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return resultSet.first() ? ModelMapper.getInstance().toVideoDTO(resultSet) : null;
    }

    @Override
    public void update(Integer id, Video video) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance()
                        .updateVideoQuery(hasTitle(video), hasUrl(video), hasText(video), hasArticle(video)))) {

            int parameterIndex = 1;

            if (hasTitle(video)) statement.setString(parameterIndex++, video.getTitle());
            if (hasUrl(video)) statement.setString(parameterIndex++, video.getUrl());
            if (hasText(video)) statement.setInt(parameterIndex++, video.getTextId());
            if (hasArticle(video)) statement.setInt(parameterIndex++, video.getArticleId());

            if (hasAnythingToUpdate(video)) {
                statement.setInt(parameterIndex, id);
                statement.executeUpdate();
            }
        }
    }

    @Override
    public void delete(Integer id) throws SQLException {
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().deleteVideoQuery())) {

            statement.setInt(1, id);
            statement.executeUpdate();
        }
    }

    @Override
    public List<VideoDTO> getForArticle(Integer id) throws SQLException {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getVideosForArticle())) {

            statement.setInt(1, id);
            resultSet = statement.executeQuery();
        }
        return ModelMapper.getInstance().toVideosDTO(resultSet);
    }

    private boolean hasTitle(Video video) {
        return video.getTitle() != null;
    }

    private boolean hasUrl(Video video) {
        return video.getUrl() != null;
    }

    private boolean hasText(Video video) {
        return video.getTextId() != null;
    }

    private boolean hasArticle(Video video) {
        return video.getArticleId() != null;
    }

    private boolean hasAnythingToUpdate(Video video) {
        return hasTitle(video) || hasUrl(video) || hasText(video) || hasArticle(video);
    }

}
