package com.example.school.repositories;

import com.example.school.models.dtos.UserDTO;
import com.example.school.models.utils.ModelMapper;
import com.example.school.repositories.utils.ConnectionWrapper;
import com.example.school.repositories.utils.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class UserRepositoryImpl implements UserRepository {
    @Override
    public UserDTO getByUsername(String username) {
        ResultSet resultSet;
        try (PreparedStatement statement = ConnectionWrapper.getInstance().getConnection()
                .prepareStatement(QueryBuilder.getInstance().getUserByUsernameQuery())) {

            statement.setString(1, username);
            resultSet = statement.executeQuery();

            return resultSet.first() ? ModelMapper.getInstance().toUserDTO(resultSet) : null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
