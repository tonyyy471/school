package com.example.school.repositories;

import com.example.school.models.dtos.UserDTO;

public interface UserRepository {
    UserDTO getByUsername(String username);
}
