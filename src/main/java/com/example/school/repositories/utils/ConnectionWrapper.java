package com.example.school.repositories.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionWrapper {
    private static ConnectionWrapper connectionWrapper;
    private Connection connection;

    private ConnectionWrapper() throws SQLException {
        initializeConnection();
    }

    public static ConnectionWrapper getInstance() throws SQLException {
        if (connectionWrapper == null)
            connectionWrapper = new ConnectionWrapper();
        return connectionWrapper;
    }

    public Connection getConnection() {
        return connection;
    }

    private void initializeConnection() throws SQLException {
        this.connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/school", "root", "1234");
    }
}
