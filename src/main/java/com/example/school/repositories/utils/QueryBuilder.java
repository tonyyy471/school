package com.example.school.repositories.utils;

public class QueryBuilder {
    private static QueryBuilder queryBuilder;

    private QueryBuilder() {
    }

    public static QueryBuilder getInstance() {
        if (queryBuilder == null)
            queryBuilder = new QueryBuilder();
        return queryBuilder;
    }


    public String saveTextQuery() {
        return "insert into texts(content)value(?)";
    }

    public String getTextQuery() {
        return "select id, content from texts where id = ?";
    }

    public String updateTextQuery() {
        return "update texts set content = ? where id = ?";
    }

    public String deleteTextQuery() {
        return "delete from texts where id = ?";
    }

    public String getCategoriesQuery() {
        return "select id, name from categories";
    }

    public String getCategoryByNameQuery() {
        return "select id, name from categories where name = ?";
    }

    public String saveCategoryQuery() {
        return "insert into categories(name)value(?)";
    }

    public String getCategoryByIdQuery() {
        return "select id, name from categories where id = ?";
    }

    public String updateCategoryQuery() {
        return "update categories set name = ? where id = ?";
    }

    public String deleteCategoryQuery() {
        return "delete from categories where id = ?";
    }

    public String saveArticleQuery(boolean hasText) {
        return hasText ? "insert into articles (timestamp, title, category_id, text_id) values (?, ?, ?, ?)" :
                "insert into articles (timestamp, title, category_id) values (?, ?, ?)";
    }

    public String getArticleByIdQuery() {
        return "select id, timestamp, title, category_id, text_id from articles where id = ?";
    }

    public String updateArticleQuery(boolean hasTitle, boolean hasCategory, boolean hasText) {
        String query = "update articles set ";

        if (hasTitle) query += "title = ?,";
        if (hasCategory) query += "category_id = ?,";
        if (hasText) query += "text_id = ?";

        if (query.endsWith(",")) query = query.substring(0, query.length() - 1);

        return query + " where id = ?";
    }

    public String deleteArticleQuery() {
        return "delete from articles where id = ?";
    }

    public String getLastFiveArticlesQuery() {
        return "select id, timestamp, title, category_id, text_id from articles order by timestamp desc limit 5";
    }

    public String getArticlesForCategoryQuery() {
        return "select id, timestamp, title, category_id, text_id from articles where category_id = ?";
    }

    public String getArticlesForMonthQuery() {
        return "select id, timestamp, title, category_id, text_id from articles where timestamp >= ? and timestamp < ?";
    }

    public String saveImageQuery(boolean hasTitle, boolean hasText) {
        String title = hasTitle ? "title," : "",
                titleQuestion = hasTitle ? "?," : "",
                text = hasText ? "text_id," : "",
                textQuestion = hasText ? "?," : "";
        return "insert into images (" + title + " file_name, " + text + " article_id) values (" + titleQuestion + "?," + textQuestion + "?)";
    }

    public String getImageByIdQuery() {
        return "select id, title, file_name,text_id,article_id from images where id = ?";
    }

    public String updateImageQuery(boolean hasTitle, boolean hasFile, boolean hasText, boolean hasArticle) {
        String query = "update images set ";

        if (hasTitle) query += "title = ?,";
        if (hasFile) query += "file_name = ?,";
        if (hasText) query += "text_id = ?,";
        if (hasArticle) query += "article_id = ?";

        if (query.endsWith(",")) query = query.substring(0, query.length() - 1);

        return query + " where id = ?";
    }

    public String deleteImageQuery() {
        return "delete from images where id = ?";
    }

    public String getImagesForArticle() {
        return "select id, title, file_name, text_id, article_id from images where article_id = ?";
    }

    public String saveDocumentQuery(boolean hasText) {
        String text = hasText ? "text_id," : "",
                textQuestion = hasText ? "?," : "";
        return "insert into documents (title, file_name, " + text + " article_id) values (?,?," + textQuestion + "?)";
    }

    public String getDocumentByIdQuery() {
        return "select id, title, file_name, text_id, article_id from documents where id = ?";
    }

    public String updateDocumentQuery(boolean hasTitle, boolean hasFile, boolean hasText, boolean hasArticle) {
        String query = "update documents set ";

        if (hasTitle) query += "title = ?,";
        if (hasFile) query += "file_name = ?,";
        if (hasText) query += "text_id = ?,";
        if (hasArticle) query += "article_id = ?";

        if (query.endsWith(",")) query = query.substring(0, query.length() - 1);

        return query + " where id = ?";
    }

    public String deleteDocumentQuery() {
        return "delete from documents where id = ?";
    }

    public String getDocumentsForArticle() {
        return "select id, title, file_name, text_id, article_id from documents where article_id = ?";
    }

    public String saveVideoQuery(boolean hasTitle, boolean hasText) {
        String title = hasTitle ? "title," : "",
                titleQuestion = hasTitle ? "?," : "",
                text = hasText ? "text_id," : "",
                textQuestion = hasText ? "?," : "";
        return "insert into videos (" + title + " url, " + text + " article_id) values (" + titleQuestion + "?," + textQuestion + "?)";
    }

    public String getVideoByIdQuery() {
        return "select id, title, url, text_id, article_id from videos where id = ?";
    }

    public String updateVideoQuery(boolean hasTitle, boolean hasUrl, boolean hasText, boolean hasArticle) {
        String query = "update videos set ";

        if (hasTitle) query += "title = ?,";
        if (hasUrl) query += "url = ?,";
        if (hasText) query += "text_id = ?,";
        if (hasArticle) query += "article_id = ?";

        if (query.endsWith(",")) query = query.substring(0, query.length() - 1);

        return query + " where id = ?";
    }

    public String deleteVideoQuery() {
        return "delete from videos where id = ?";
    }

    public String getVideosForArticle() {
        return "select id, title, url, text_id, article_id from videos where article_id = ?";
    }

    public String getUserByUsernameQuery() {
        return "select id, username, password from users where username = ?";
    }
}
