package com.example.school.repositories;

import com.example.school.models.Category;
import com.example.school.models.dtos.CategoryDTO;

import java.sql.SQLException;
import java.util.List;

public interface CategoryRepository {
    List<CategoryDTO> getAll() throws SQLException;

    CategoryDTO getByName(Category category) throws SQLException;

    void save(Category category) throws SQLException;

    CategoryDTO getById(Integer id) throws SQLException;

    void update(Integer id, Category category) throws SQLException;

    void delete(Integer id) throws SQLException;
}
